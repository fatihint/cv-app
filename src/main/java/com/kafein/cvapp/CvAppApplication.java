package com.kafein.cvapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

@SpringBootApplication
//@EnableWebMvc
public class CvAppApplication {
    public static void main(String[] args) {
        SpringApplication.run(CvAppApplication.class, args);
    }
}
