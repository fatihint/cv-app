package com.kafein.cvapp.controller;

import com.kafein.cvapp.util.SessionUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;

@Controller
@RequestMapping(value = {"/", "/login"})
public class LoginController {

    @Autowired
    private SessionUtil sessionUtil;

    @GetMapping
    public ModelAndView showLoginIndex(HttpServletRequest request) {

        if(sessionUtil.isLoggedIn()){
                return new ModelAndView("redirect:/home");
        }

        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("login");
        return modelAndView;

    }



}
