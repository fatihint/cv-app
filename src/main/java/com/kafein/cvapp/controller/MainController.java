package com.kafein.cvapp.controller;

import com.kafein.cvapp.util.SessionUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.Set;
import java.util.stream.Collectors;

@Controller
public class MainController {

    @Autowired
    private SessionUtil sessionUtil;

    @RequestMapping(value = "/home")
    public ModelAndView loginRedirect(HttpServletRequest request) {

        sessionUtil.hasAdminRole();
        sessionUtil.getUsername();

        ModelAndView modelAndView = new ModelAndView();

        modelAndView.setViewName("home");

        return modelAndView;
    }

    @RequestMapping(value = "/admin")
    public ModelAndView adminHome(HttpServletRequest request) {

        ModelAndView modelAndView = new ModelAndView();

        modelAndView.setViewName("/admin/home");

        return modelAndView;
    }

    @RequestMapping(value = "/home/profile")
    public ModelAndView showProfile(HttpServletRequest request) {

        ModelAndView modelAndView = new ModelAndView();

        modelAndView.setViewName("profile");

        return modelAndView;
    }

    @RequestMapping(value = "/user")
    public ModelAndView userHome(HttpServletRequest request) {

        ModelAndView modelAndView = new ModelAndView();

        modelAndView.setViewName("/user/home");

        return modelAndView;
    }
}
