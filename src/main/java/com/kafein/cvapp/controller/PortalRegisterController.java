package com.kafein.cvapp.controller;

import com.kafein.cvapp.dto.PortalRegisterDTO;
import com.kafein.cvapp.dto.UserRegisterResponseDTO;
import com.kafein.cvapp.service.PortalRegisterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;

@RestController
public class PortalRegisterController {

    @Autowired
    private PortalRegisterService portalRegisterService;

    @RequestMapping(value = "portalregister", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE,produces = {"application/json"})
    public ResponseEntity<UserRegisterResponseDTO> add(@RequestBody PortalRegisterDTO input) {

        return new ResponseEntity<UserRegisterResponseDTO>(new UserRegisterResponseDTO(portalRegisterService.sendPostToService(input)), HttpStatus.OK);

    }

}
