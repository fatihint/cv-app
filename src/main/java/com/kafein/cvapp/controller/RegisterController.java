package com.kafein.cvapp.controller;

import com.kafein.cvapp.model.User;
import com.kafein.cvapp.service.UserServiceImpl;
import com.kafein.cvapp.util.SessionUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;

@Controller
@RequestMapping(value = "/register")
public class RegisterController {

    @Autowired
    UserServiceImpl userServiceImpl;

    @Autowired
    SessionUtil sessionUtil;

    @GetMapping
    public ModelAndView showRegisterIndex() {

        if (sessionUtil.isLoggedIn()) {
            return new ModelAndView("redirect:/home");
        }
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("user", new User());
        modelAndView.setViewName("register");
        return modelAndView;
    }

    @PostMapping
    /*

    @TODO Aktivasyon maili

     */
    public ModelAndView registerUser(@Valid User user, BindingResult bindingResult) {

        ModelAndView modelAndView = new ModelAndView();

        if (!bindingResult.hasErrors()) {
            if (!userServiceImpl.saveUser(user)) {
                bindingResult.rejectValue("email", "error.user", "Bu email zaten kayitli");
            } else {
                return new ModelAndView("redirect:/login");
            }
        }

        modelAndView.setViewName("register");
        return modelAndView;
    }

}
