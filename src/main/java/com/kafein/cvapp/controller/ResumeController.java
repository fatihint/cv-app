package com.kafein.cvapp.controller;

import com.kafein.cvapp.domain.ResumeSearchCriteria;
import com.kafein.cvapp.model.CV;
import com.kafein.cvapp.model.CVTechnology;
import com.kafein.cvapp.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

@Controller
public class ResumeController {

    @Autowired
    CVServiceImpl cvService;

    @Autowired
    StatusServiceImpl statusService;

    @Autowired
    PositionServiceImpl positionService;

    @Autowired
    TechnologyServiceImpl technologyService;

    @Autowired
    CVTechnologyServiceImpl cvTechnologyService;

    @ModelAttribute
    public ResumeSearchCriteria criteria() {
        return new ResumeSearchCriteria();
    }

    @RequestMapping(value = "/home/resumes", method = RequestMethod.GET)
    public ModelAndView showResumes(@ModelAttribute("resumeSearchCriteria") ResumeSearchCriteria criteria) {

        ModelAndView modelAndView = new ModelAndView();

        modelAndView.addObject("statuses", statusService.findAll());
        modelAndView.addObject("positions", positionService.findAll());
        modelAndView.addObject("technologies", technologyService.findAll());
        modelAndView.addObject("searchResults", criteria);
        List<CV> cvs = cvService.findAll();

        modelAndView.addObject("cvs", cvs);
        modelAndView.setViewName("resumes");

        return modelAndView;
    }

    @RequestMapping(value = "/home/resume", method = RequestMethod.GET)
    public ModelAndView showResume(@RequestParam(value = "id", required = false) String id) {

        ModelAndView modelAndView = new ModelAndView();
        CV cv;
        modelAndView.addObject("statuses", statusService.findAll());
        modelAndView.addObject("positions", positionService.findAll());
        modelAndView.addObject("technologies", technologyService.findAll());
//        modelAndView.addObject("cvTechnologies", cvTechnologyService.findAll());

        if (id == null) {
            cv = new CV();
            modelAndView.addObject("cv", cv);
        } else {
            cv = cvService.findById(Integer.parseInt(id));
            if (cv != null) {
                modelAndView.addObject("cv", cv);
            } else {
                return new ModelAndView("redirect:/home/resumes");
            }
        }

        modelAndView.setViewName("resume");
        return modelAndView;
    }

    @RequestMapping(value = "/home/resume", method = RequestMethod.POST)
    public ModelAndView saveResume(@Validated CV cv, BindingResult bindingResult) {

        /*
        @TODO: Validator
         */


        if (bindingResult.hasErrors()) {
            return new ModelAndView();
        }
        CV cv1 = cvService.save(cv);
        return new ModelAndView("redirect:/home/resume?id=" + cv1.getId());

//        if (positionService.saveResume(position)) {
//        }

    }

    @RequestMapping(value = "/home/resume", method = RequestMethod.DELETE)
    public ModelAndView deleteResume(@ModelAttribute CV cv) {
        cvService.delete(cv);
        return new ModelAndView("redirect:/home/resumes");
    }


}
