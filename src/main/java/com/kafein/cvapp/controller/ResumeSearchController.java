package com.kafein.cvapp.controller;

import com.kafein.cvapp.domain.ResumeSearchCriteria;
import com.kafein.cvapp.model.CV;
import com.kafein.cvapp.service.CVServiceImpl;
import com.kafein.cvapp.service.PositionService;
import com.kafein.cvapp.service.PositionServiceImpl;
import com.kafein.cvapp.service.StatusServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

@Controller
public class ResumeSearchController {

    @Autowired
    CVServiceImpl cvService;

    @Autowired
    StatusServiceImpl statusService;

    @Autowired
    PositionServiceImpl positionService;

    @ModelAttribute
    public ResumeSearchCriteria criteria() {
        return new ResumeSearchCriteria();
    }

    @RequestMapping(value = "/home/resumes", method = RequestMethod.POST)
    public ModelAndView showResumes(@ModelAttribute("resumeSearchCriteria") ResumeSearchCriteria criteria) {
        ModelAndView modelAndView = new ModelAndView();
        List<CV> cvs = cvService.search(criteria);
        modelAndView.addObject("statuses", statusService.findAll());
        modelAndView.addObject("positions", positionService.findAll());
        modelAndView.addObject("cvs", cvs);
        modelAndView.setViewName("resumes");
        return modelAndView;
    }



}
