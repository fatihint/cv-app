package com.kafein.cvapp.controller.admin;

import com.kafein.cvapp.model.Position;
import com.kafein.cvapp.service.PositionServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

@Controller
@RequestMapping(value = "home/admin")
public class PositionController {

    @Autowired
    PositionServiceImpl positionService;

    @RequestMapping(value = "/positions", method = RequestMethod.GET)
    public ModelAndView position() {

        ModelAndView modelAndView = new ModelAndView();

        List<Position> positions = positionService.findAll();

        modelAndView.addObject("positions", positions);
        modelAndView.setViewName("/admin/positions");

        return modelAndView;

    }

    @RequestMapping(value = "/position", method = RequestMethod.GET)
    public ModelAndView showPosition(@RequestParam(value = "id", required = false) String id) {

        ModelAndView modelAndView = new ModelAndView();
        Position position;

        if (id == null) {
            position = new Position();
            modelAndView.addObject("position", position);
        } else {
            position = positionService.findById(Integer.parseInt(id));
            if(position != null) {
                modelAndView.addObject("position", position);
            } else {
                return new ModelAndView("redirect:/home/admin/position");
            }
        }

        modelAndView.setViewName("/admin/position");
        return modelAndView;

    }

    @RequestMapping(value = "/position", method = RequestMethod.POST)
    public ModelAndView savePosition(@ModelAttribute Position position) {

        /*
        @TODO: Validator
         */


        Position position1 = positionService.save(position);
        return new ModelAndView("redirect:/home/admin/position?id=" + position1.getId());

//        if (positionService.savePosition(position)) {
//        }

    }

    @RequestMapping(value = "/position", method = RequestMethod.DELETE)
    public ModelAndView deletePosition(@ModelAttribute Position position) {
        positionService.delete(position);
        return new ModelAndView("redirect:/home/admin/positions");
    }


}
