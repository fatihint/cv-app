package com.kafein.cvapp.controller.admin;

import com.kafein.cvapp.model.Position;
import com.kafein.cvapp.model.Status;
import com.kafein.cvapp.service.PositionServiceImpl;
import com.kafein.cvapp.service.StatusServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

@Controller
@RequestMapping(value = "home/admin")
public class StatusController {

    @Autowired
    StatusServiceImpl statusService;

    @RequestMapping(value = "/statuses", method = RequestMethod.GET)
    public ModelAndView status() {

        ModelAndView modelAndView = new ModelAndView();

        List<Status> statuses = statusService.findAll();

        modelAndView.addObject("statuses", statuses);
        modelAndView.setViewName("/admin/statuses");

        return modelAndView;

    }

    @RequestMapping(value = "/status", method = RequestMethod.GET)
    public ModelAndView showStatus(@RequestParam(value = "id", required = false) String id) {

        ModelAndView modelAndView = new ModelAndView();
        Status status;

        if (id == null) {
            status = new Status();
            modelAndView.addObject("status", status);
        } else {
            status = statusService.findById(Integer.parseInt(id));
            if(status != null) {
                modelAndView.addObject("status", status);
            } else {
                return new ModelAndView("redirect:/home/admin/status");
            }
        }

        modelAndView.setViewName("/admin/status");
        return modelAndView;

    }

    @RequestMapping(value = "/status", method = RequestMethod.POST)
    public ModelAndView saveStatus(@ModelAttribute Status status) {

        /*
        @TODO: Validator
         */


        Status status1 = statusService.save(status);
        return new ModelAndView("redirect:/home/admin/status?id=" + status1.getId());

//        if (statusService.saveStatus(status)) {
//        }

    }

    @RequestMapping(value = "/status", method = RequestMethod.DELETE)
    public ModelAndView deleteStatus(@ModelAttribute Status status) {
        statusService.delete(status);
        return new ModelAndView("redirect:/home/admin/statuses");
    }


}
