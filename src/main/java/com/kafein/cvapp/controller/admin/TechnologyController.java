package com.kafein.cvapp.controller.admin;

import com.kafein.cvapp.model.CVTechnology;
import com.kafein.cvapp.model.Technology;
import com.kafein.cvapp.service.TechnologyServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

@Controller
@RequestMapping(value = "home/admin")
public class TechnologyController {

    @Autowired
    TechnologyServiceImpl technologyService;

    @RequestMapping(value = "/technologies", method = RequestMethod.GET)
    public ModelAndView technology() {

        ModelAndView modelAndView = new ModelAndView();

        List<Technology> technologies = technologyService.findAll();

        modelAndView.addObject("technologies", technologies);
        modelAndView.setViewName("/admin/technologies");

        return modelAndView;

    }

    @RequestMapping(value = "/technology", method = RequestMethod.GET)
    public ModelAndView showTechnology(@RequestParam(value = "id", required = false) String id) {

        ModelAndView modelAndView = new ModelAndView();
        Technology technology;

        if (id == null) {
            technology = new Technology();
            modelAndView.addObject("technology", technology);
        } else {
            technology = technologyService.findById(Integer.parseInt(id));
            if(technology != null) {
                modelAndView.addObject("technology", technology);
            } else {
                return new ModelAndView("redirect:/home/admin/technology");
            }
        }

        modelAndView.setViewName("/admin/technology");
        return modelAndView;

    }

    @RequestMapping(value = "/technology", method = RequestMethod.POST)
    public ModelAndView saveTechnology(@ModelAttribute Technology technology) {

        /*
        @TODO: Validator
         */


        Technology technology1 = technologyService.save(technology);
        return new ModelAndView("redirect:/home/admin/technology?id=" + technology1.getId());

//        if (technologyService.saveTechnology(technology)) {
//        }

    }

    @RequestMapping(value = "/technology", method = RequestMethod.DELETE)
    public ModelAndView deleteTechnology(@ModelAttribute Technology technology) {
        technologyService.delete(technology);
        return new ModelAndView("redirect:/home/admin/technologies");
    }


}
