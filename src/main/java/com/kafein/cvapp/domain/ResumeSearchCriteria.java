package com.kafein.cvapp.domain;

import com.kafein.cvapp.model.Position;
import com.kafein.cvapp.model.Status;

import java.util.Collection;
import java.util.List;

public class ResumeSearchCriteria {

    private String firstName;
    private String lastName;
    private Status status;
    private Collection<Position> positions;

    public Collection<Position> getPositions() {
        return positions;
    }

    public void setPositions(Collection<Position> positions) {
        this.positions = positions;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
}
