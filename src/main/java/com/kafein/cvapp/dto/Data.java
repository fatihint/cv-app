
package com.kafein.cvapp.dto;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "Id",
    "Mail",
    "FirstName",
    "LastName",
    "ImagePath",
    "Birthday",
    "StartDateofEmployee",
    "MobilePhoneNumber",
    "WorkPhoneNumber",
    "Roles"
})
public class Data {

    @JsonProperty("Id")
    private Integer id;
    @JsonProperty("Mail")
    private String mail;
    @JsonProperty("FirstName")
    private String firstName;
    @JsonProperty("LastName")
    private String lastName;
    @JsonProperty("ImagePath")
    private String imagePath;
    @JsonProperty("Birthday")
    private String birthday;
    @JsonProperty("StartDateofEmployee")
    private String startDateofEmployee;
    @JsonProperty("MobilePhoneNumber")
    private Object mobilePhoneNumber;
    @JsonProperty("WorkPhoneNumber")
    private Object workPhoneNumber;
    @JsonProperty("Roles")
    private List<Role> roles = null;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("Id")
    public Integer getId() {
        return id;
    }

    @JsonProperty("Id")
    public void setId(Integer id) {
        this.id = id;
    }

    @JsonProperty("Mail")
    public String getMail() {
        return mail;
    }

    @JsonProperty("Mail")
    public void setMail(String mail) {
        this.mail = mail;
    }

    @JsonProperty("FirstName")
    public String getFirstName() {
        return firstName;
    }

    @JsonProperty("FirstName")
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    @JsonProperty("LastName")
    public String getLastName() {
        return lastName;
    }

    @JsonProperty("LastName")
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    @JsonProperty("ImagePath")
    public String getImagePath() {
        return imagePath;
    }

    @JsonProperty("ImagePath")
    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    @JsonProperty("Birthday")
    public String getBirthday() {
        return birthday;
    }

    @JsonProperty("Birthday")
    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    @JsonProperty("StartDateofEmployee")
    public String getStartDateofEmployee() {
        return startDateofEmployee;
    }

    @JsonProperty("StartDateofEmployee")
    public void setStartDateofEmployee(String startDateofEmployee) {
        this.startDateofEmployee = startDateofEmployee;
    }

    @JsonProperty("MobilePhoneNumber")
    public Object getMobilePhoneNumber() {
        return mobilePhoneNumber;
    }

    @JsonProperty("MobilePhoneNumber")
    public void setMobilePhoneNumber(Object mobilePhoneNumber) {
        this.mobilePhoneNumber = mobilePhoneNumber;
    }

    @JsonProperty("WorkPhoneNumber")
    public Object getWorkPhoneNumber() {
        return workPhoneNumber;
    }

    @JsonProperty("WorkPhoneNumber")
    public void setWorkPhoneNumber(Object workPhoneNumber) {
        this.workPhoneNumber = workPhoneNumber;
    }

    @JsonProperty("Roles")
    public List<Role> getRoles() {
        return roles;
    }

    @JsonProperty("Roles")
    public void setRoles(List<Role> roles) {
        this.roles = roles;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
