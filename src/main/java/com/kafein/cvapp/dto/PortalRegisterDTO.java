package com.kafein.cvapp.dto;

import java.io.Serializable;

public class PortalRegisterDTO implements Serializable {

    private String Email;
    private String Password;

    public PortalRegisterDTO() {

    }

    public PortalRegisterDTO(String email, String password) {
        Email = email;
        Password = password;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public String getPassword() {
        return Password;
    }

    public void setPassword(String password) {
        Password = password;
    }
}
