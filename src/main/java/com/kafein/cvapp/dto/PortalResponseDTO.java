
package com.kafein.cvapp.dto;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "Messages",
    "Success",
    "Data",
    "SavedId",
    "StatusCode"
})
public class PortalResponseDTO {

    @JsonProperty("Messages")
    private List<Object> messages = null;
    @JsonProperty("Success")
    private Boolean success;
    @JsonProperty("Data")
    private Data data;
    @JsonProperty("SavedId")
    private Integer savedId;
    @JsonProperty("StatusCode")
    private Integer statusCode;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("Messages")
    public List<Object> getMessages() {
        return messages;
    }

    @JsonProperty("Messages")
    public void setMessages(List<Object> messages) {
        this.messages = messages;
    }

    @JsonProperty("Success")
    public Boolean getSuccess() {
        return success;
    }

    @JsonProperty("Success")
    public void setSuccess(Boolean success) {
        this.success = success;
    }

    @JsonProperty("Data")
    public Data getData() {
        return data;
    }

    @JsonProperty("Data")
    public void setData(Data data) {
        this.data = data;
    }

    @JsonProperty("SavedId")
    public Integer getSavedId() {
        return savedId;
    }

    @JsonProperty("SavedId")
    public void setSavedId(Integer savedId) {
        this.savedId = savedId;
    }

    @JsonProperty("StatusCode")
    public Integer getStatusCode() {
        return statusCode;
    }

    @JsonProperty("StatusCode")
    public void setStatusCode(Integer statusCode) {
        this.statusCode = statusCode;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
