
package com.kafein.cvapp.dto;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "RoleId",
    "DepartmentId",
    "IsDefaultRole",
    "RoleName",
    "DepartmentName",
    "isTeamLeadApprovalRequired",
    "UserRoleAuthorization"
})
public class Role {

    @JsonProperty("RoleId")
    private Integer roleId;
    @JsonProperty("DepartmentId")
    private Integer departmentId;
    @JsonProperty("IsDefaultRole")
    private Boolean isDefaultRole;
    @JsonProperty("RoleName")
    private String roleName;
    @JsonProperty("DepartmentName")
    private String departmentName;
    @JsonProperty("isTeamLeadApprovalRequired")
    private Boolean isTeamLeadApprovalRequired;
    @JsonProperty("UserRoleAuthorization")
    private UserRoleAuthorization userRoleAuthorization;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("RoleId")
    public Integer getRoleId() {
        return roleId;
    }

    @JsonProperty("RoleId")
    public void setRoleId(Integer roleId) {
        this.roleId = roleId;
    }

    @JsonProperty("DepartmentId")
    public Integer getDepartmentId() {
        return departmentId;
    }

    @JsonProperty("DepartmentId")
    public void setDepartmentId(Integer departmentId) {
        this.departmentId = departmentId;
    }

    @JsonProperty("IsDefaultRole")
    public Boolean getIsDefaultRole() {
        return isDefaultRole;
    }

    @JsonProperty("IsDefaultRole")
    public void setIsDefaultRole(Boolean isDefaultRole) {
        this.isDefaultRole = isDefaultRole;
    }

    @JsonProperty("RoleName")
    public String getRoleName() {
        return roleName;
    }

    @JsonProperty("RoleName")
    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    @JsonProperty("DepartmentName")
    public String getDepartmentName() {
        return departmentName;
    }

    @JsonProperty("DepartmentName")
    public void setDepartmentName(String departmentName) {
        this.departmentName = departmentName;
    }

    @JsonProperty("isTeamLeadApprovalRequired")
    public Boolean getIsTeamLeadApprovalRequired() {
        return isTeamLeadApprovalRequired;
    }

    @JsonProperty("isTeamLeadApprovalRequired")
    public void setIsTeamLeadApprovalRequired(Boolean isTeamLeadApprovalRequired) {
        this.isTeamLeadApprovalRequired = isTeamLeadApprovalRequired;
    }

    @JsonProperty("UserRoleAuthorization")
    public UserRoleAuthorization getUserRoleAuthorization() {
        return userRoleAuthorization;
    }

    @JsonProperty("UserRoleAuthorization")
    public void setUserRoleAuthorization(UserRoleAuthorization userRoleAuthorization) {
        this.userRoleAuthorization = userRoleAuthorization;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
