package com.kafein.cvapp.dto;

public class UserRegisterResponseDTO {

    private boolean success;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public UserRegisterResponseDTO(boolean success) {
        this.success = success;
    }
}
