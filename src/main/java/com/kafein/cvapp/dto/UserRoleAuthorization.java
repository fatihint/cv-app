
package com.kafein.cvapp.dto;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "roleId",
    "departmentId",
    "showPage",
    "addNews",
    "addEducation",
    "addOpenPosition",
    "assignRoleDepartman",
    "showKalitePortal",
    "createAdvertisement"
})
public class UserRoleAuthorization {

    @JsonProperty("roleId")
    private Integer roleId;
    @JsonProperty("departmentId")
    private Integer departmentId;
    @JsonProperty("showPage")
    private Boolean showPage;
    @JsonProperty("addNews")
    private Boolean addNews;
    @JsonProperty("addEducation")
    private Boolean addEducation;
    @JsonProperty("addOpenPosition")
    private Boolean addOpenPosition;
    @JsonProperty("assignRoleDepartman")
    private Boolean assignRoleDepartman;
    @JsonProperty("showKalitePortal")
    private Boolean showKalitePortal;
    @JsonProperty("createAdvertisement")
    private Boolean createAdvertisement;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("roleId")
    public Integer getRoleId() {
        return roleId;
    }

    @JsonProperty("roleId")
    public void setRoleId(Integer roleId) {
        this.roleId = roleId;
    }

    @JsonProperty("departmentId")
    public Integer getDepartmentId() {
        return departmentId;
    }

    @JsonProperty("departmentId")
    public void setDepartmentId(Integer departmentId) {
        this.departmentId = departmentId;
    }

    @JsonProperty("showPage")
    public Boolean getShowPage() {
        return showPage;
    }

    @JsonProperty("showPage")
    public void setShowPage(Boolean showPage) {
        this.showPage = showPage;
    }

    @JsonProperty("addNews")
    public Boolean getAddNews() {
        return addNews;
    }

    @JsonProperty("addNews")
    public void setAddNews(Boolean addNews) {
        this.addNews = addNews;
    }

    @JsonProperty("addEducation")
    public Boolean getAddEducation() {
        return addEducation;
    }

    @JsonProperty("addEducation")
    public void setAddEducation(Boolean addEducation) {
        this.addEducation = addEducation;
    }

    @JsonProperty("addOpenPosition")
    public Boolean getAddOpenPosition() {
        return addOpenPosition;
    }

    @JsonProperty("addOpenPosition")
    public void setAddOpenPosition(Boolean addOpenPosition) {
        this.addOpenPosition = addOpenPosition;
    }

    @JsonProperty("assignRoleDepartman")
    public Boolean getAssignRoleDepartman() {
        return assignRoleDepartman;
    }

    @JsonProperty("assignRoleDepartman")
    public void setAssignRoleDepartman(Boolean assignRoleDepartman) {
        this.assignRoleDepartman = assignRoleDepartman;
    }

    @JsonProperty("showKalitePortal")
    public Boolean getShowKalitePortal() {
        return showKalitePortal;
    }

    @JsonProperty("showKalitePortal")
    public void setShowKalitePortal(Boolean showKalitePortal) {
        this.showKalitePortal = showKalitePortal;
    }

    @JsonProperty("createAdvertisement")
    public Boolean getCreateAdvertisement() {
        return createAdvertisement;
    }

    @JsonProperty("createAdvertisement")
    public void setCreateAdvertisement(Boolean createAdvertisement) {
        this.createAdvertisement = createAdvertisement;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
