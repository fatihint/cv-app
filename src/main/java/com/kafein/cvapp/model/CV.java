package com.kafein.cvapp.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import java.util.Collection;

@Entity
public class CV extends AuditModel {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @Column(name = "firstname")
    private String firstName;

    @Column(name = "lastname")
    private String lastName;

    @Column
    private String sex;

    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "cv_positions",
                joinColumns = @JoinColumn(name = "cv_id", referencedColumnName = "id"),
                inverseJoinColumns = @JoinColumn(name = "position_id", referencedColumnName = "id"))
    private Collection<Position> positions;


    @OneToMany(mappedBy = "pk.cv", cascade = CascadeType.ALL)
    private Collection<CVTechnology> cvTechnologies;

    public Collection<CVTechnology> getCvTechnologies() {
        return cvTechnologies;
    }

    public void setCvTechnologies(Collection<CVTechnology> cvTechnologies) {
        this.cvTechnologies = cvTechnologies;
    }
    //    @ManyToMany(cascade = CascadeType.ALL)
//    @JoinTable(name = "cv_technologies",
//                joinColumns = @JoinColumn(name = "cv_id", referencedColumnName = "id"),
//                inverseJoinColumns = @JoinColumn(name = "technology_id", referencedColumnName = "id"))
//    private Collection<Technology> technologies;

    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "cv_references",
                joinColumns = @JoinColumn(name = "cv_id", referencedColumnName = "id"),
                inverseJoinColumns = @JoinColumn(name = "user_id", referencedColumnName = "id"))
    private Collection<User> users;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "status_id")
    private Status status;


    public CV() {
    }


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public Collection<User> getUsers() {
        return users;
    }

    public void setUsers(Collection<User> users) {
        this.users = users;
    }

    public Collection<Position> getPositions() {
        return positions;
    }

    public void setPositions(Collection<Position> positions) {
        this.positions = positions;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }


}
