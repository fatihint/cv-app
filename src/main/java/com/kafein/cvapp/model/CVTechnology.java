package com.kafein.cvapp.model;

import javax.persistence.*;

@Entity
@Table(name = "cv_technologies")
@AssociationOverrides({
        @AssociationOverride(name = "pk.technology", joinColumns = @JoinColumn(name = "technology_id")),
        @AssociationOverride(name = "pk.cv", joinColumns = @JoinColumn(name = "cv_id"))})
public class CVTechnology{

    @EmbeddedId
    private CVTechnologyId pk = new CVTechnologyId();

    @Column
    private String experience;

    public CVTechnology() {

    }

    @Transient
    public Technology getTechnology() {
        return getCvTechnologyId().getTechnology();
    }

    public void setTechnology(Technology technology) {
        getCvTechnologyId().setTechnology(technology);
    }

    @Transient
    public CV getCV() {
        return getCvTechnologyId().getCv();
    }

    public void setCV(CV cv) {
        getCvTechnologyId().setCv(cv);
    }

    public CVTechnologyId getCvTechnologyId() {
        return pk;
    }

    public void setCvTechnologyId(CVTechnologyId pk) {
        this.pk = pk;
    }

    public String getExperience() {
        return experience;
    }

    public void setExperience(String experience) {
        this.experience = experience;
    }

}
