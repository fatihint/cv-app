package com.kafein.cvapp.model;

import javax.persistence.*;
import java.util.Collection;

@Entity
public class Technology extends AuditModel{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column
    private String name;

    @OneToMany(mappedBy = "pk.technology", cascade = CascadeType.ALL)
    private Collection<CVTechnology> cvTechnologies;

    public Technology() {
    }

    public Technology(String name) {
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Collection<CVTechnology> getCvTechnologies() {
        return cvTechnologies;
    }

    public void setCvTechnologies(Collection<CVTechnology> cvTechnologies) {
        this.cvTechnologies = cvTechnologies;
    }
}
