package com.kafein.cvapp.model;

import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import java.util.Collection;

@Entity
@Table(name = "users")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column
    private boolean active;

    @Column
    @NotEmpty(message = "İsim alanı boş bırakılamaz.")
    @Size(min = 3, max = 63, message = "İsim uzunluğu {min} - {max} karakter arasında olmalıdır.")
    private String name;

    @Column
    @NotEmpty(message = "Soyad alanı boş bırakılamaz")
    @Size(min = 3, max = 63, message = "Soyad uzunluğu {min} - {max} karakter arasında olmalıdır.")
    private String lastname;

    @Column
    @NotEmpty(message = "E-mail alanı boş bırakılamaz.")
    @Email(message = "Lütfen geçerli bir e-mail adresi giriniz.")
    private String email;

    @Column
    @Length(min = 6, message = "Parola minimum {min} karakter olmalıdır.")
    @NotEmpty(message = "Parola boş bırakılamaz.")
    private String password;

    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinTable(name = "users_roles",
                joinColumns = @JoinColumn(name = "user_id", referencedColumnName = "id"),
                inverseJoinColumns = @JoinColumn(name = "role_id", referencedColumnName = "id"))
    @JoinColumn(name = "role_id")
    private Collection<Role> roles;

    public User() {

    }

    public User(boolean active, @NotEmpty(message = "İsim alanı boş bırakılamaz.") @Size(min = 3, max = 63, message = "İsim uzunluğu {min} - {max} karakter arasında olmalıdır.") String name, @NotEmpty(message = "Soyad alanı boş bırakılamaz") @Size(min = 3, max = 63, message = "Soyad uzunluğu {min} - {max} karakter arasında olmalıdır.") String lastname, @NotEmpty(message = "E-mail alanı boş bırakılamaz.") @Email(message = "Lütfen geçerli bir e-mail adresi giriniz.") String email, @Length(min = 6, message = "Parola minimum {min} karakter olmalıdır.") @NotEmpty(message = "Parola boş bırakılamaz.") String password, Collection<Role> roles) {
        this.active = active;
        this.name = name;
        this.lastname = lastname;
        this.email = email;
        this.password = password;
        this.roles = roles;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Collection<Role> getRoles() {
        return roles;
    }

    public void setRoles(Collection<Role> roles) {
        this.roles = roles;
    }

}
