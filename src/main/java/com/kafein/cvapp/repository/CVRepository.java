package com.kafein.cvapp.repository;

import com.kafein.cvapp.domain.ResumeSearchCriteria;
import com.kafein.cvapp.model.CV;
import com.kafein.cvapp.model.Position;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CVRepository extends CrudRepository<CV, Integer> {

//    Position findByName(String name);
    List<CV> findAll();
    CV findTopById(int id);
    void delete(CV cv);

}
