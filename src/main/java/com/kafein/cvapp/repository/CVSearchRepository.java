package com.kafein.cvapp.repository;

import com.kafein.cvapp.domain.ResumeSearchCriteria;
import com.kafein.cvapp.model.CV;
import com.kafein.cvapp.model.Position;
import com.kafein.cvapp.model.Status;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.springframework.stereotype.Repository;
import org.springframework.util.Assert;
import org.springframework.util.StringUtils;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

@Repository
public class CVSearchRepository {

    @PersistenceContext
    private EntityManager entityManager;

    public List<CV> findCVS(ResumeSearchCriteria resumeSearchCriteria) {
        Assert.notNull(resumeSearchCriteria, "Search Criteria are required!");
        CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        CriteriaQuery<CV> query = builder.createQuery(CV.class);
        Root<CV> cv = query.from(CV.class);
        List<Predicate> predicates = new ArrayList<>();

        if (StringUtils.hasText(resumeSearchCriteria.getFirstName())) {
            String firstName = resumeSearchCriteria.getFirstName().toUpperCase();
            predicates.add(builder.like(builder.upper(cv.get("firstName")), "%" + firstName + "%"));
        }

        if (StringUtils.hasText(resumeSearchCriteria.getLastName())) {
            String lastName = resumeSearchCriteria.getLastName().toUpperCase();
            predicates.add(builder.like(builder.upper(cv.get("lastName")), "%" + lastName + "%"));
        }

        if (resumeSearchCriteria.getStatus() != null) {
            Status status = entityManager.find(Status.class, resumeSearchCriteria.getStatus().getId());
            predicates.add(builder.equal(cv.<Status> get("status"), status));
        }

        if (resumeSearchCriteria.getPositions() != null) {
//            List<Position> positions = entityManager.find(Position.class, resumeSearchCriteria.getPositions());
//          predicates.add(builder.equal(cv.<Position> get("positions")), positions);
        }

        if (!predicates.isEmpty()) {
            query.where(predicates.toArray(new Predicate[predicates.size()]));
        }

        query.orderBy(builder.asc(cv.get("id")));
        return entityManager.createQuery(query).getResultList();
    }

}
