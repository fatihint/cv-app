package com.kafein.cvapp.repository;

import com.kafein.cvapp.model.CV;
import com.kafein.cvapp.model.CVTechnology;
import com.kafein.cvapp.model.Technology;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface CVTechnologyRepository extends CrudRepository<CVTechnology, Integer> {
    List<CVTechnology> findAll();
//    CVTechnology findByCVAndTechnology(CV cv, Technology technology);
}
