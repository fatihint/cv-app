package com.kafein.cvapp.repository;

import com.kafein.cvapp.model.Position;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PositionRepository extends CrudRepository<Position, Integer> {

    List<Position> findAll();
    Position findTopById(int id);
    Position findByName(String name);
    void delete(Position position);
}
