package com.kafein.cvapp.repository;

import com.kafein.cvapp.model.Status;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface StatusRepository extends CrudRepository<Status, Integer> {

    List<Status> findAll();
    Status findTopById(int id);
    Status findByName(String name);
    void delete(Status position);
}
