package com.kafein.cvapp.repository;

import com.kafein.cvapp.model.Position;
import com.kafein.cvapp.model.Technology;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TechnologyRepository extends CrudRepository<Technology, Integer> {

    List<Technology> findAll();
    Technology findTopById(int id);
    Technology findByName(String name);
    void delete(Technology position);
}
