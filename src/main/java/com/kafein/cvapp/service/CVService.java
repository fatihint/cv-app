package com.kafein.cvapp.service;

import com.kafein.cvapp.domain.ResumeSearchCriteria;
import com.kafein.cvapp.model.CV;

import java.util.List;

public interface CVService {

    CV findById(int id);
    List<CV> findAll();
    CV save(CV cv);
    void delete(CV cv);
    List<CV> search(ResumeSearchCriteria resumeSearchCriteria);
}
