package com.kafein.cvapp.service;

import com.kafein.cvapp.domain.ResumeSearchCriteria;
import com.kafein.cvapp.model.CV;
import com.kafein.cvapp.model.Position;
import com.kafein.cvapp.model.User;
import com.kafein.cvapp.repository.CVRepository;
import com.kafein.cvapp.repository.CVSearchRepository;
import com.kafein.cvapp.repository.PositionRepository;
import com.kafein.cvapp.util.SessionUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Service
public class CVServiceImpl implements CVService{

    @Autowired
    CVRepository cvRepository;

    @Autowired
    CVSearchRepository searchRepository;

    @Autowired
    SessionUtil sessionUtil;

    @Autowired
    UserServiceImpl userService;

    @Override
    public List<CV> findAll() {
        return cvRepository.findAll();
    }

    @Override
    public CV findById(int id) {
        return cvRepository.findTopById(id);
    }

    @Override
    public CV save(CV cv) {
        User user = userService.findByEmail(sessionUtil.getUsername());
        ArrayList<User> users = new ArrayList<>();
        users.add(user);
        cv.setUsers(users);
        return cvRepository.save(cv);
    }

    @Override
    public void delete(CV cv) {
        cvRepository.delete(cv);
    }

    @Override
    public List<CV> search(ResumeSearchCriteria resumeSearchCriteria) {
        return searchRepository.findCVS(resumeSearchCriteria);
    }
}
