package com.kafein.cvapp.service;

import com.kafein.cvapp.model.CV;
import com.kafein.cvapp.model.CVTechnology;
import com.kafein.cvapp.model.Technology;

import java.util.List;

public interface CVTechnologyService {

    List<CVTechnology> findAll();
//    CVTechnology findByCvAndTechnology(CV cv, Technology technology);

}
