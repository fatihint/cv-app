package com.kafein.cvapp.service;

import com.kafein.cvapp.model.CV;
import com.kafein.cvapp.model.CVTechnology;
import com.kafein.cvapp.model.Technology;
import com.kafein.cvapp.repository.CVTechnologyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CVTechnologyServiceImpl implements CVTechnologyService {

    @Autowired
    CVTechnologyRepository cvTechnologyRepository;

    @Override
    public List<CVTechnology> findAll() {
        return cvTechnologyRepository.findAll();
    }

//    @Override
//    public CVTechnology findByCvAndTechnology(CV cv, Technology technology) {
////        return cvTechnologyRepository.findByCVAndTechnology(cv,technology);
//    }
}
