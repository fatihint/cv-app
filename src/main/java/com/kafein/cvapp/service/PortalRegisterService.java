package com.kafein.cvapp.service;

import com.kafein.cvapp.dto.PortalRegisterDTO;
import com.kafein.cvapp.dto.PortalResponseDTO;
import com.kafein.cvapp.model.Role;
import com.kafein.cvapp.model.User;
import com.kafein.cvapp.repository.RoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;

@Service
public class PortalRegisterService {

    @Autowired
    private UserServiceImpl userServiceImpl;

    @Autowired
    private RoleServiceImpl roleServiceImpl;

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    public boolean sendPostToService(PortalRegisterDTO input) {

        User user = userServiceImpl.findByEmail(input.getEmail());

        RestTemplate rt;
        String uri = new String("http://portal.kafein.com.tr/v1/User/GetUserByMailAndPassword");
        PortalRegisterDTO registerDTO;

        if (user == null) {
            rt = new RestTemplate();
            rt.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
            rt.getMessageConverters().add(new StringHttpMessageConverter());

            registerDTO = new PortalRegisterDTO();

            // Input.email
            // registerDTO.setEmail("sozersadun@gmail.com");
            // registerDTO.setPassword("2gm4gndm");

            registerDTO.setEmail(input.getEmail());
            registerDTO.setPassword(input.getPassword());

            ResponseEntity<PortalResponseDTO> response = rt.postForEntity(uri, registerDTO, PortalResponseDTO.class);

            if (response.getBody().getSuccess()) {
                return saveUserFromPortal(response.getBody(), input, user);
            }

            return false;

        } else if(!(input.getPassword().equals(user.getPassword()))) {
            // check if portal pass changed
            rt = new RestTemplate();
            rt.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
            rt.getMessageConverters().add(new StringHttpMessageConverter());

            registerDTO = new PortalRegisterDTO();

            registerDTO.setEmail(input.getEmail());
            registerDTO.setPassword(input.getPassword());

            ResponseEntity<PortalResponseDTO> response = rt.postForEntity(uri, registerDTO, PortalResponseDTO.class);

            if (response.getBody().getSuccess()) {
                return saveUserFromPortal(response.getBody(), input, user);
            }
        }

        return false;
    }


    private boolean saveUserFromPortal(PortalResponseDTO response, PortalRegisterDTO input, User user) {

        if (user == null) {
            user = new User();
        }

        user.setEmail(response.getData().getMail());
        user.setPassword(bCryptPasswordEncoder.encode(input.getPassword()));
        user.setName(response.getData().getFirstName());
        user.setLastname(response.getData().getLastName());
        List<Role> roles = new ArrayList<>();
        if (response.getData().getRoles().get(0).getDepartmentId() == 1) {
            roles.add(roleServiceImpl.findByName("IK"));
        } else {
            roles.add(roleServiceImpl.findByName("ADMIN"));
        }
        user.setRoles(roles);
        user.setActive(true);
        userServiceImpl.registerPortalUser(user);

        return true;
    }


}
