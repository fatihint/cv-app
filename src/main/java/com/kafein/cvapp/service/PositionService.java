package com.kafein.cvapp.service;

import com.kafein.cvapp.model.Position;
import com.kafein.cvapp.model.User;

import java.util.List;

public interface PositionService {

    List<Position> findAll();
    Position findByName(String name);
    Position findById(int id);
    Position save(Position position);
    void delete(Position position);

}
