package com.kafein.cvapp.service;

import com.kafein.cvapp.model.Position;
import com.kafein.cvapp.repository.PositionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PositionServiceImpl implements PositionService{

    @Autowired
    PositionRepository positionRepository;

    public List<Position> findAll() {
        return positionRepository.findAll();
    }

    public Position findByName(String name) {
        return positionRepository.findByName(name);
    }

    @Override
    public Position findById(int id) {
        return positionRepository.findTopById(id);
    }

    @Override
    public Position save(Position position) {
        return positionRepository.save(position);

    }

    @Override
    public void delete(Position position) {
        positionRepository.delete(position);
    }
}
