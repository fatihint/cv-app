package com.kafein.cvapp.service;

import com.kafein.cvapp.model.Role;
import com.kafein.cvapp.model.User;

public interface RoleService {
    Role findByName(String name);
}
