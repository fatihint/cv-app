package com.kafein.cvapp.service;

import com.kafein.cvapp.model.Role;
import com.kafein.cvapp.model.User;
import com.kafein.cvapp.repository.RoleRepository;
import com.kafein.cvapp.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.HashSet;


@Service
public class RoleServiceImpl implements RoleService {


    @Autowired
    private RoleRepository roleRepository;


    @Override
    public Role findByName(String name) {
        return roleRepository.findByName(name);
    }
}
