package com.kafein.cvapp.service;

import com.kafein.cvapp.model.Position;
import com.kafein.cvapp.model.Status;

import java.util.List;

public interface StatusService {

    List<Status> findAll();
    Status findByName(String name);
    Status findById(int id);
    Status save(Status status);
    void delete(Status status);

}
