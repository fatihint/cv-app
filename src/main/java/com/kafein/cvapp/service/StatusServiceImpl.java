package com.kafein.cvapp.service;

import com.kafein.cvapp.model.Status;
import com.kafein.cvapp.repository.StatusRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class StatusServiceImpl implements StatusService{

    @Autowired
    StatusRepository statusRepository;

    public List<Status> findAll() {
        return statusRepository.findAll();
    }

    public Status findByName(String name) {
        return statusRepository.findByName(name);
    }

    @Override
    public Status findById(int id) {
        return statusRepository.findTopById(id);
    }

    @Override
    public Status save(Status status) {
        return statusRepository.save(status);

    }

    @Override
    public void delete(Status status) {
        statusRepository.delete(status);
    }
}
