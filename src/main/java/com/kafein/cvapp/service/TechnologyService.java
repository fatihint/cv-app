package com.kafein.cvapp.service;

import com.kafein.cvapp.model.CVTechnology;
import com.kafein.cvapp.model.Technology;

import java.util.List;

public interface TechnologyService {

    List<Technology> findAll();
    Technology findByName(String name);
    Technology findById(int id);
    Technology save(Technology technology);
    void delete(Technology technology);

}
