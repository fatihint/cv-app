package com.kafein.cvapp.service;

import com.kafein.cvapp.model.CVTechnology;
import com.kafein.cvapp.model.Position;
import com.kafein.cvapp.model.Technology;
import com.kafein.cvapp.repository.TechnologyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class TechnologyServiceImpl implements TechnologyService{

    @Autowired
    TechnologyRepository technologyRepository;

    public List<Technology> findAll() {

         return technologyRepository.findAll();

//        List<CVTechnology> cvTechnologies = new ArrayList<>();
//        List<Technology> technologies = technologyRepository.findAll();
//        for (Technology technology : technologies) {
//            CVTechnology cvTechnology = new CVTechnology();
//            cvTechnology.setTechnology(technology);
//            cvTechnologies.add(cvTechnology);
//        }
    }

    public Technology findByName(String name) {
        return technologyRepository.findByName(name);
    }

    @Override
    public Technology findById(int id) {
        return technologyRepository.findTopById(id);
    }

    @Override
    public Technology save(Technology technology) {
        return technologyRepository.save(technology);

    }

    @Override
    public void delete(Technology technology) {
        technologyRepository.delete(technology);
    }
}
