package com.kafein.cvapp.service;

import com.kafein.cvapp.model.User;

public interface UserService {

    boolean saveUser(User user);
    User findByEmail(String email);
    boolean registerPortalUser(User user);
}
