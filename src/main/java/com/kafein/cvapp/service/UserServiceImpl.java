package com.kafein.cvapp.service;

import com.kafein.cvapp.model.Role;
import com.kafein.cvapp.model.User;
import com.kafein.cvapp.repository.RoleRepository;
import com.kafein.cvapp.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.HashSet;


@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Override
    public boolean registerPortalUser(User user) {
        userRepository.save(user);
        return true;
    }

    @Override
    public boolean saveUser(User user) {

        User user1 = userRepository.findByEmail(user.getEmail());
        if (user1 == null) {
            Role role = roleRepository.findByName("ADMIN");
            user.setRoles(new HashSet<Role>(Arrays.asList(role)));
            user.setActive(true);
            user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
            userRepository.save(user);
            return true;
        } else {
            return false;
        }
    }

    @Override
    public User findByEmail(String email) {
        return userRepository.findByEmail(email);
    }

}
