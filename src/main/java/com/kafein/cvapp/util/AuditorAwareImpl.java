package com.kafein.cvapp.util;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.AuditorAware;

import java.util.Optional;

public class AuditorAwareImpl implements AuditorAware<String> {

    @Autowired
    SessionUtil sessionUtil;

    @Override
    public Optional<String> getCurrentAuditor() {
        return Optional.of(sessionUtil.getUsername());
    }
}
